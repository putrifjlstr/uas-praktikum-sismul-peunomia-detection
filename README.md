# UAS Praktikum Sismul - pneumonia detection



## Pneumonia Detection menggunakan X-tray images

Pneumonia merupakan salah satu penyakit paru-paru yang sering menyebabkan kematian. Deteksi dini dan akurat sangat penting dalam mengatasi penyakit ini. Dalam penelitian ini, kami mengusulkan sebuah aplikasi deteksi pneumonia yang menggunakan deep learning pada citra chest X-ray. Metode yang pada penelitian ini menggunakan Convolutional Neural Network (CNN) untuk melatih model yang mampu mengklasifikasikan citra X-ray apakah terdapat tanda-tanda pneumonia atau tidak. Pada penelitian ini diggunakan dataset yang terdiri dari ribuan citra X-ray yang dikumpulkan dari website kaggle. Dataset ini telah dikurasi dan dianotasi oleh ahli radiologi untuk memastikan keakuratan hasil. Pada tahap pra-pemrosesan, kami melakukan normalisasi intensitas piksel dan augmentasi data untuk meningkatkan keragaman dataset. Selanjutnya, kami membangun dan melatih model CNN dengan menggunakan arsitektur yang telah terbukti efektif dalam pengolahan citra. Proses pelatihan dilakukan dengan membagi dataset menjadi subset pelatihan dan validasi, serta mengoptimasi parameter-model menggunakan algoritma penurunan gradien stokastik. Pada pengujian lebih lanjut, kami menerapkan model pada aplikasi deteksi pneumonia yang memungkinkan pengguna mengunggah citra chest X-ray mereka sendiri dan memperoleh hasil deteksi dalam waktu nyata. Aplikasi ini dapat digunakan oleh tenaga medis untuk melakukan skrining awal dan mendukung proses diagnosis pneumonia.

pada aplikasi ini saya mengambil dataset yang ada pada kaggle

- [Link Dataset](https://www.kaggle.com/datasets/paultimothymooney/chest-xray-pneumonia)
- [Link Paper](https://drive.google.com/drive/folders/1Fh--7T9AxesYc2xwTPoGMwXniaDS5vtV?usp=drive_link)


## Preprocessing

Tahapan preprocessing data dalam konteks pemrosesan citra pada deteksi pneumonia dengan citra X-Ray menggunakan deep learning dapat mencakup beberapa langkah berikut:

1. Pengumpulan Data:
   - Kumpulkan dataset citra X-Ray yang mencakup kasus positif pneumonia dan kasus negatif (tanpa pneumonia). 

2. Pemisahan Dataset:
   - Bagi dataset menjadi subset pelatihan (training set), validasi (validation set), dan pengujian (test set) dengan proporsi yang sesuai. Hal ini penting untuk melatih, mengevaluasi, dan menguji model dengan benar.
   
3. Pembersihan Data:
   - Identifikasi dan hapus data yang tidak lengkap, data duplikat, atau data yang tidak relevan.
   - Periksa kualitas citra dan hilangkan citra yang buram, terdistorsi, atau tidak memadai.

4. Normalisasi:
   - Lakukan normalisasi intensitas piksel untuk memastikan setiap citra memiliki rentang nilai yang serupa. Ini dapat dilakukan dengan melakukan normalisasi skala piksel menjadi rentang 0 hingga 1 atau menggunakan teknik normalisasi lainnya.

5. Resolusi dan Dimensi:
   - Sesuaikan resolusi dan dimensi citra agar seragam dalam dataset. Hal ini dapat dilakukan dengan mengubah ukuran citra ke dimensi yang sama atau menggunakan teknik resizing yang sesuai.

6. Augmentasi Data:
   - Terapkan augmentasi data pada citra untuk meningkatkan keragaman dataset dan mengurangi overfitting. Beberapa teknik augmentasi yang umum digunakan meliputi rotasi, pemotongan, pergeseran, flipping, zooming, dan perubahan kecerahan kontras.

7. Ekstraksi Fitur:
   - Jika diperlukan, lakukan ekstraksi fitur untuk mendapatkan fitur-fitur yang lebih representatif dari citra X-Ray. Teknik ekstraksi fitur seperti Histogram of Oriented Gradients (HOG), Local Binary Patterns (LBP), atau Convolutional Neural Networks (CNN) dapat digunakan.

8. Pembagian Data:
   - Pisahkan citra dan labelnya menjadi input (citra) dan output (label) terpisah yang sesuai dengan format yang diterima oleh model deep learning.




## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/putrifjlstr/uas-praktikum-sismul-peunomia-detection/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README
Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
